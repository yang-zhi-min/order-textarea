# 具有行号的有序文本域(适用于VUE3，风格基于ElementPlus)。

![示例图片](https://gitee.com/yang-zhi-min/order-textarea/raw/master/src/packages/demo.png)

## 1. 安装

```
npm install order-textarea
```

## 2. 在组件中使用

```
<template>
    <order-textarea
      ref="orderTextareaRef"
      v-model="value"
      placeholder="请输入内容，每行一个"
      :row-height="32"
      :fixed-rows="undefined"
      :min-rows="2"
      :max-rows="10"
      :delete-blank-rows="false"
      :merge-repeat-rows="false"
      :limit-rows="0"
      :disabled="false"
      @focus="onFocus"
      @input="onInput"
      @blur="onBlur">
        <template #order="{number}">
            #{{ number }}
        </template>
        <template #placeholder>
            <div class="order-textarea__placeholder">
                自定义Placeholder
            </div>
            <div class="order-textarea__placeholder">
                <i>请输入内容，每行一个</i>
            </div>
        </template>
    </order-textarea>
    
    <div class="props">
        placeholder: 占位符，可使用slot自定义。默认为"请输入内容，每行一个"。
        rowHeight: 每行的高度。默认为32px。
        fixedRows: 固定行数，不设置此参数，行数会根据内容自适应。默认为undefined。
        minRows: 根据内容自适应行数的最小行数。如果设置了fixRows，minRows会被忽略。默认为2。
        maxRows: 根据内容自适应行数的最大行数，如果设置了fixRows，maxRows会被忽略。默认为10。
        trim: 是否去除每行的首尾空格。默认为false。
        deleteBlankRows: 是否删除空白行。默认为false。
        mergeRepeatRows: 是否合并重复行。默认为false。
        limitRows: 限制最大输入行数，超出的行数将被截断。默认为0，不限制。
        disabled: 是否禁用。默认为false。
        @focus: 当文本域获得焦点时触发。
        @input: 当文本域内容发生变化时触发。
        @blur: 当文本域失去焦点时触发。
    </div>
    <div class="exposes">
        scrollToRow(number, offset): 滚动到指定行。number: 行号; offset?: 滚动上下偏移量，默认为0。
    </div>
</template>

<script setup>
    import {ref} from "vue"
    import OrderTextarea from "order-textarea"

    const value = ref([])
    const orderTextareaRef = ref(null)
    
    function onFocus() {
        console.log("onFocus")
    }
    
    function onInput(val) {
        console.log("onInput", val)
    }

    function onBlur() {
        console.log("onBlur")
    }

    function scrollToRow() {
        orderTextareaRef.value.scrollToRow(50, 0)
    }
</script>

<style scoped>
    .order-textarea__placeholder{
        padding-left: 40px;
    }
</style>
```