import OrderTextarea from "./index.vue"

OrderTextarea.name = "OrderTextarea"

const install = App => {
    App.component(OrderTextarea.name, OrderTextarea)
}

const windowObj = window
if (typeof windowObj !== "undefined" && windowObj.Vue) {
    install(windowObj.Vue)
}

export default OrderTextarea