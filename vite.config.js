import {defineConfig} from "vite"
import vue from "@vitejs/plugin-vue"
import libCss from "vite-plugin-libcss"
import {resolve} from "path"

export default defineConfig({
    plugins: [
        vue(),
        libCss(),
    ],
    resolve: {
        alias: [
            {
                find: "@",
                replacement: resolve(__dirname, "src")
            },
            {
                find: "@assets",
                replacement: resolve(__dirname, "src/assets")
            },
            {
                find: "@c",
                replacement: resolve(__dirname, "src/components")
            },
        ],
    },
    build: {
        lib: {
            entry: resolve(__dirname, "src/packages/index.js"),
            name: "OrderTextarea",
            fileName: `index`,
        },
        rollupOptions: {
            external: [
                "vue",
            ],
            output: {
                globals: {
                    vue: "Vue"
                },
            },
        },
    },
})
